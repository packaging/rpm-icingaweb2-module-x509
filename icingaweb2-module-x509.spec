# Certificate Monitoring (x509) Module for Icinga Web 2 | (c) 2018-2019 Icinga Development Team <info@icinga.com> | GPLv2+

%global revision 1
%global module_name x509

%global icingaweb_min_version 2.6.0

Name:           icingaweb2-module-%{module_name}
Version:        1.0.0
Release:        %{revision}%{?dist}
Summary:        Certificate Monitoring (x509) - Icinga Web 2 module
Group:          Applications/System
License:        GPLv2+
URL:            https://icinga.com
Source0:        https://github.com/Icinga/icingaweb2-module-%{module_name}/archive/v%{version}.tar.gz
Source1:        %{service_name}.service
#/icingaweb2-module-%{module_name}-%{version}.tar.gz
BuildArch:      noarch

%global basedir %{_datadir}/icingaweb2/modules/%{module_name}
%global service_name icinga-%{module_name}

%if "%{_vendor}" == "suse"
%global service_user wwwrun
%else # suse
%global service_user apache
%endif # suse

BuildRequires:  systemd-devel
Requires:       systemd

Requires:       icingaweb2 >= %{icingaweb_min_version}
Requires:       php-Icinga >= %{icingaweb_min_version}

Requires:       openssl

%{?suse_version:Requires:   php-gmp}

Requires:       icingaweb2-module-ipl >= 0.1
Requires:       icingaweb2-module-reactbundle >= 0.4

%description
The certificate monitoring module for Icinga keeps track of certificates as
they are deployed in a network environment. It does this by scanning networks
for TLS services and collects whatever certificates it finds along the way.
The certificates are verified using its own trust store.

The module’s web frontend can be used to view scan results, allowing you to
drill down into detailed information about any discovered certificate of your
landscape.

%prep
%setup -q
#-n icingaweb2-module-%{module_name}-%{version}

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{basedir}

cp -r * %{buildroot}%{basedir}

install -d %{buildroot}%{_unitdir}
install -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}/%{service_name}.service

# Replace user in service unit
sed -i -e 's~^User=.*~User=%{service_user}~' %{buildroot}%{_unitdir}/%{service_name}.service

%if "%{_vendor}" == "suse"
install -d %{buildroot}%{_sbindir}
ln -sf /usr/sbin/service %{buildroot}%{_sbindir}/rc%{service_name}
%endif # suse

%clean
rm -rf %{buildroot}

%pre
%if "%{_vendor}" == "suse"
  %service_add_pre %{service_name}.service
%endif # suse

exit 0

%post
set -e

%if "%{_vendor}" == "suse"
  %service_add_post %{service_name}.service
%else # suse
%systemd_post %{service_name}.service
%endif # suse

exit 0

%preun
set -e

%if "%{_vendor}" == "suse"
  %service_del_preun %{service_name}.service
%else # suse
  %systemd_preun %{service_name}.service
%endif # suse

# Only for removal
if [ $1 == 0 ]; then
    echo "Disabling icingaweb2 module '%{module_name}'"
    rm -f /etc/icingaweb2/enabledModules/%{module_name}
fi

exit 0

%postun
set -e

%if "%{_vendor}" == "suse"
  %service_del_postun %{service_name}.service
%else # suse
  %systemd_postun_with_restart %{service_name}.service
%endif # suse

exit 0

%files
%doc README.md LICENSE

%defattr(-,root,root)
%{basedir}

%{_unitdir}/%{service_name}.service

%if "%{_vendor}" == "suse"
%{_sbindir}/rc%{service_name}
%endif # suse

%changelog
* Fri Aug 30 2019 Markus Frosch <markus.frosch@icinga.com> - 1.0.0-1
- Initial package version
